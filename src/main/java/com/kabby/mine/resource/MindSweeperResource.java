package com.kabby.mine.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kabby.mine.model.Mine;
import com.kabby.mine.service.MineService;

@RestController
@CrossOrigin("*")
public class MindSweeperResource {
	
	
	@Autowired
	private MineService mineService;
	
	@GetMapping(value = "/mine")
	public String[][] getMineSweeper(@RequestParam(name="mineNumber") int mineNumber, 
			                      @RequestParam(name="mineFieldXY") int mineFieldXY){
		
		Mine mine=new Mine(mineNumber, mineFieldXY, mineFieldXY);
		mineService.setMine(mine.getMineField(), mine.getMineFieldX(), mine.getMineFieldY(), mine.getMineNumber());
		return mineService.setMineHint(mine.getMineField(), mine.getMineFieldX(), mine.getMineFieldY(), mine.getMineNumber());


	}
	

}
