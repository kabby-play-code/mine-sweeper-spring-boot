package com.kabby.mine.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Mine {

	private int mineNumber;   //지뢰의 갯수
	private int mineFieldX; //지뢰 찾기를 시작할 경기장 크기 : X
	private int mineFieldY; //지뢰 찾기를 시작할 경기장 크기 : Y
	private String[][] mineField;  //지뢰 찾기를 시작할 경기장 크기
	
	
	
	public Mine(int mineNumber, int mineFieldX,int mineFieldY) {
		this.mineNumber=mineNumber;
		this.mineFieldX=mineFieldX;
		this.mineFieldY=mineFieldY;
		this.mineField=new String[mineFieldX][mineFieldY];
	}
	

	

	
	
	
	
}
