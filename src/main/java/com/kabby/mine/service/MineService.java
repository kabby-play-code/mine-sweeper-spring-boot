package com.kabby.mine.service;

public interface MineService {
	
	/**
	 * 
	 * @param mineField   지뢰 찾기를 시작할 경기장 크기
	 * @param mineFieldX  지뢰 찾기를 시작할 경기장 크기 : X
	 * @param mineFieldY  지뢰 찾기를 시작할 경기장 크기 : Y
	 * @param mineNumber  지뢰의 갯수
	 * 
	 * @return 무작위 지뢰를 담은 2차원 배열을 반환.
	 */
	public String[][] setMine(String[][] mineField, int mineFieldX, int mineFieldY,int mineNumber);
		
	/**
	 * 
	 * @param mineField   지뢰 찾기를 시작할 경기장 크기
	 * @param mineFieldX  지뢰 찾기를 시작할 경기장 크기 : X
	 * @param mineFieldY  지뢰 찾기를 시작할 경기장 크기 : Y
	 * @param mineNumber  지뢰의 갯수
	 * 
	 * @return 각 지뢰 제외한 주변 8개의 숫자를 담은 2차원 배열 값을 반환.
	 */
	public String[][] setMineHint(String[][] mineField, int mineFieldX, int mineFieldY,int mineNumber);
	


}
