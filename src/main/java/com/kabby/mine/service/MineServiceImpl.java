package com.kabby.mine.service;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MineServiceImpl implements MineService {
	
	private static final String MINE_STR = "(@)";
	private static final Logger logger = LoggerFactory.getLogger(MineServiceImpl.class);

	
	/**
	 * 
	 * @param mineField   지뢰 찾기를 시작할 경기장 크기
	 * @param mineFieldX  지뢰 찾기를 시작할 경기장 크기 : X
	 * @param mineFieldY  지뢰 찾기를 시작할 경기장 크기 : Y
	 * @param mineNumber  지뢰의 갯수
	 * 
	 * @return 무작위 지뢰를 담은 2차원 배열을 반환.
	 */
	@Override
	public String[][] setMine(String[][] mineField, int mineFieldX, int mineFieldY,int mineNumber) {
		logger.info("지뢰: " +mineNumber);
		Random random = new Random();
		int x = random.nextInt(mineFieldY);
		int y = random.nextInt(mineFieldX);
		
		
		if(MINE_STR.equals(mineField[x][y])) {
			logger.info("똑같이 좌표에 지뢰가 있습니다. 새로운 좌표에 지뢰를 지정합니다.");
		}else if(!MINE_STR.equals(mineField[x][y])) {
			mineField[x][y] = MINE_STR;
			mineNumber--;
			
		}		
		
		
		if(mineNumber == 0) {
			
			for(int i=0; i<mineFieldX; i++) {
				for(int j=0; j<mineFieldY; j++) {
					if(mineField[i][j]!=MINE_STR){
						mineField[i][j]="0";
					}
				}
			}
			logger.info("지뢰 모두 세팅 되었습니다.");

			return mineField;
		}else {
			
			setMine(mineField, mineFieldX, mineFieldY, mineNumber);
			
		}
		
		return mineField;
		
		
		
	}


	/**
	 * 
	 * @param mineField   지뢰 찾기를 시작할 경기장 크기
	 * @param mineFieldX  지뢰 찾기를 시작할 경기장 크기 : X
	 * @param mineFieldY  지뢰 찾기를 시작할 경기장 크기 : Y
	 * @param mineNumber  지뢰의 갯수
	 * 
	 * @return 각 지뢰 제외한 주변 8개의 숫자를 담은 2차원 배열 값을 반환.
	 */
	@Override
	public String[][] setMineHint(String[][] mineField, int mineFieldX, int mineFieldY, int mineNumber) {
		//
		
		
		/*
		 * X와 Y 크기만큼 for문을 돌린 후 
		 * 현재의 위치가 지뢰일 경우 라면 continue을 사용해서 넘어간 후
		 * 
		 * 지뢰의 주변 위치를 탐색해서 주변의 숫자를 증가 시킨다.
		 * 
		 */
		for(int i = 0; i < mineFieldX; i++) 
        {
          for(int j = 0; j < mineFieldY; j++) 
          {      
              if(mineField[i][j] == MINE_STR) 
              {
                  continue;
              }
              int mineCheck =0;
                  for(int k = i-1; k <= i+1; k++)
                  {
                      for(int h = j-1; h <= j+1; h++)
                      {
                          if(i==k && j==h)
                          {
                              continue; 
                          }
                          if(k==-1 || k==mineFieldX || h==-1 || h==mineFieldY)
                          {
                              continue; 
                          }
                          if(mineField[k][h] == MINE_STR)
                          {
                        	  mineCheck++;
                          }
                      }
                  }
                  mineField[i][j] = Integer.toString(mineCheck);
              }
         }
		
		
		/*
		 * 콘솔에 지뢰찾기 부분을 보기 쉽게 하기 위해서 System.out.print와 System.out.println을 씀. 
		 * 
		 */
		for(int i=0; i<mineFieldX; i++) {
			for(int j=0; j<mineFieldY; j++) {
				
				System.out.print(mineField[i][j] + "\t");
			}
			System.out.println("\n");
		}
		
		logger.info("지뢰와 더불어 모든 사각형에 숫자가 표시 되었습니다.");
		
		return mineField;
	}



}
