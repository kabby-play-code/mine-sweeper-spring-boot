package com.kabby.mine;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.kabby.mine.model.Mine;
import com.kabby.mine.service.MineService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MinesweeperApplicationTests {

	@Autowired
	private MineService mineService;
	
	@Test
	public void contextLoads() {
		
		int mineNumber = 10;
		int mineFieldX = 10;
		int mineFieldY = 10;
		
		Mine mine=new Mine(mineNumber, mineFieldX, mineFieldY);
		
		mineService.setMine(mine.getMineField(), mine.getMineFieldX(), mine.getMineFieldY(), mine.getMineNumber());		
		mineService.setMineHint(mine.getMineField(), mine.getMineFieldX(), mine.getMineFieldY(), mine.getMineNumber());
		
	}

}
